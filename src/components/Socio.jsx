import React, { Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Badge, Button } from 'react-bootstrap';

const Socio = ({socio, borrarSocio}) => {
    return ( 
        <Fragment>
            <Badge variant="secondary">
                <p>Socio: {socio.id}</p>
                <p>Nombre: {socio.nombre}</p>
                <p>DNI: {socio.dni}</p>
            <Button 
                variant="light"
                onClick={() => borrarSocio(socio.id)}
            >Borrar Socio
            </Button>
            </Badge>
            <br/>
        </Fragment>
     );
}
 
export default Socio;