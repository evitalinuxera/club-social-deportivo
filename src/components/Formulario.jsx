import React, {Fragment, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Button } from 'react-bootstrap';
import uuid from 'uuid/dist/v4';

const Formulario = ({agregarSocio}) => {

// Creo un socio vacío y lo inicializo con su state

const [socio, editarSocio] = useState({
    nombre:"",
    dni:""
});

// Creo otro state para el error del formulario

const [error, setError] = useState(false);

// Extraer los valores 

const {nombre, dni} = socio;


// Recoger lo que la persona escribe en el formulario

const handleChange = (e) => {
        editarSocio({
            ...socio,
            [e.target.name]: e.target.value,
        }
        )
};

// Cuando se envía el formulario

const submitForm = (e) => {
    e.preventDefault();

    // Validar el formulario
        if (nombre.trim() === '' || dni.trim() === ''){
            setError(true);
            return;
        }

    // Quitar el mensaje de error
        setError(false);

    // Poner un id
        socio.id = uuid();
        console.log(socio);

    // Generar el socio
        agregarSocio(socio);

    // Limpiar el form
    editarSocio({
        nombre:"",
        dni:""
    })


};


    return ( 
        <Fragment>
            {
                error
                ? <h4>Completá todos los campos</h4>
                : null
            }
               <Form
                    onSubmit={submitForm}
               >
                    <Form.Group>
                        <Form.Label>Nombre</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Nombre completo"
                            name="nombre" 
                            onChange={handleChange}
                            value={nombre}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>DNI</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Sin puntos ni espacios" 
                            name="dni"
                            onChange={handleChange}
                            value={dni}
                        />
                    </Form.Group>
                    
                    <Button 
                        variant="danger" 
                        type="submit">
                        Ingresar Socio
                    </Button>
               </Form> 
        </Fragment>
     );
}
 
export default Formulario;