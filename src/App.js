import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import React, {Fragment, useEffect, useState} from 'react';
import Formulario from './components/Formulario';
import Socio from './components/Socio';



function App() {
  

// Inicializar el local Storage

let sociosIniciales = JSON.parse(localStorage.getItem('asociados'));
if (!sociosIniciales){
  sociosIniciales=[]
};

// Generar un state vacío con los asociados

const [asociados, editarAsociados] = useState(sociosIniciales);

// Hook useEffect sirve para ejecutar cosas cuando está todo listo
// o cuando cambia el estado

useEffect(() => {
  if (sociosIniciales) {
    localStorage.setItem('asociados', JSON.stringify(asociados));
  }else{
    localStorage.setItem('asociados', JSON.stringify([])); 
  }
}, [sociosIniciales]);

// Función que toma el socio nuevo y lo mete en el array de asociados

const agregarSocio = (socio) => {
    editarAsociados([
      ...asociados,
      socio
    ])
};


// Función para borrar el socio
// No olvidar el parámetro id en el arrow

const borrarSocio = (id) => {
  const nuevosSocios = asociados.filter (socio => socio.id !== id);
  editarAsociados(nuevosSocios);
};

// Cambiar el título haya o no socios

let titulo = asociados.length === 0 ? "Aún no hay socios" : "Listado de socios";


  return (
    <Fragment>
      <Container>
        <Row>
          <Col>Club Social y Deportivo</Col>
        </Row>
        <Row>
          <Col>
            <Formulario
            agregarSocio={agregarSocio}
            />
          </Col>
          <Col>
            {titulo}
            {asociados.map( socio => 
              <Socio
              socio={socio}
              key={socio.id}
              borrarSocio={borrarSocio}
              />
              )}
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}

export default App;
